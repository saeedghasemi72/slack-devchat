import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

var config = {
  apiKey: "AIzaSyB50JlC20XY8kUOb4f24jStXJU6ZNymM2Y",
  authDomain: "react-slack-clone-1b425.firebaseapp.com",
  databaseURL: "https://react-slack-clone-1b425.firebaseio.com",
  projectId: "react-slack-clone-1b425",
  storageBucket: "react-slack-clone-1b425.appspot.com",
  messagingSenderId: "226783205302"
};
firebase.initializeApp(config);

export default firebase;
