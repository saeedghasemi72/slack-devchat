import { combineReducers } from "redux";
import User_reducer from "./User_reducer";

const rootReducer = combineReducers({
  user: User_reducer
});

export default rootReducer;
